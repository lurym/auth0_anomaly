'use strict';

var assert = require('assert'),
    ipstorage = require('../ipstorage');

function verifyIP(ip, valid, blockedList) {
    var res = ipstorage.verifyIPAddress(ip);
    assert.equal(res.valid, valid);
    assert.equal(res.blockedBy, blockedList);
}

ipstorage.refreshData(function () {
    verifyIP('127.0.0.1', false, 'cidr_report_bogons.netset');
    verifyIP('127.0.0.112334', false, null);
    verifyIP('1.2.4.5', true, null);
    console.log("TESTS PASSED");
});
