'use strict';

var http = require('http'),
    express = require('express'),
    ipstorage = require('./ipstorage');

var app = express();

//NOTE: this function should be accessible locally or in internal network only 
app.post('/private/refresh', function (req, res) {
    ipstorage.refreshData();
    res.writeHead(200);
    res.end();
});

app.get('/public/ipv4/:ipAddr', function (req, res) {
    var ipAddr = req.params.ipAddr;
    console.log('Retrieving info about ip address: ' + ipAddr);
    var verificationResult = ipstorage.verifyIPAddress(ipAddr);
    if (!verificationResult.valid && verificationResult.blockedBy === null) {
        console.log('IP address is invalid');
        res.writeHead(400);
        res.end();
        return;
    }
    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end(JSON.stringify(verificationResult));
});

ipstorage.refreshData(function () {
    app.listen(8080, "0.0.0.0");
});
