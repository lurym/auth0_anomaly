'use strict';

var iptrie = require('iptrie'),
    ip = require('ip'),
    fs = require('fs'),
    async = require('async'),
    LineByLineReader = require('line-by-line'),
    lookup = new iptrie.IPTrie(),
    blockedIpDirs = '../blocklist-ipsets/';

var blockedLists = [
        'feodo.ipset',
        'iblocklist_abuse_palevo.netset',
        'sslbl.ipset',
        'zeus_badips.ipset',
        'dshield.netset',
        'spamhaus_drop.netset',
        'cidr_report_bogons.netset',
        'blocklist_de.ipset'
    ];

function processSingleLine(newLookup, listName) {
    return function (line) {
        // check if this is a comment
        if (line.trim().startsWith('#')) {
            return;
        }
        var ipNetmask = line.split('/');
        if (!ip.isV4Format(ipNetmask[0])) {
            console.log("Invalid IP: " + line);
            return;
        }
        if (ipNetmask.length === 2) {
            var netmask = Number(ipNetmask[1]);
            if (netmask > 0 && netmask <= 32) {
                newLookup.add(ipNetmask[0], netmask, listName);
            } else {
                console.log('Invalid netmask in line: ' + line);
            }
            return;
        }
        if (ipNetmask.length === 1) {
            newLookup.add(ipNetmask[0], 32, listName);
            return;
        }
        console.log("Invalid line: " + line);
    };
}

exports.verifyIPAddress = function (ipAddr) {
    if (!ip.isV4Format(ipAddr)) {
        return {valid: false, blockedBy: null};
    }
    var blockedList = lookup.find(ipAddr);
    if (typeof blockedList === 'undefined') {
        return {valid: true, blockedBy: null};
    }
    return {valid: false, blockedBy: blockedList};
};

exports.refreshData = function (refreshDataCb) {
    console.log('Refreshing the data');
    var noOfFilesProcessed = 0,
        newLookup = new iptrie.IPTrie();
    async.eachSeries(blockedLists, function (listName, cb) {
        var filePath = blockedIpDirs + listName,
            lr = new LineByLineReader(filePath);
        lr.on('line', processSingleLine(newLookup, listName));
        lr.on('end', function () {
            noOfFilesProcessed += 1;
            if (noOfFilesProcessed === blockedLists.length) {
                console.log("All files processed. Updating lookup");
                lookup = newLookup;
                if (typeof refreshDataCb !== 'undefined') {
                    refreshDataCb();
                }
            }
            cb();
        });
    });
};
