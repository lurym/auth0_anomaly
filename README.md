# README #

Exercise description: https://auth0.slack.com/files/marcos/F6PNKRGH5/Anomaly_Detection_Exercise

### Running the code ###

```
$> npm install
$> node service.js
```
Service starts on port 8080 on default.

WARNING: Code assumes that there is blocklist-ipsets already pulled in directory next to anomaly_service
```
.
├── auth0_anomaly
└── blocklist-ipsets
```

### Testing the code ###

```
$> curl --fail http://localhost:8080/public/ipv4/1.2.3.4  
{"valid":true,"blocked_by":null}

$> curl --fail http://localhost:8080/public/ipv4/127.0.0.112334
curl: (22) The requested URL returned error: 400 Bad Request

$> curl --fail http://localhost:8080/public/ipv4/127.0.0.1
{"valid":false,"blocked_by":"cidr_report_bogons.netset"}                                                                                                                                                  
```

### Adding task to crontab ###
```
$> crontab -e
```
Add following line. This will run refresh task every 9 minutes.
```
*/9 * * * * /path/to/file/update_ipsets.sh >/dev/null 2>&1
```
