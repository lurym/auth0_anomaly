#!/usr/bin/env bash 

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

#This should be run by cron periodically. Example:
# crontab -e   or   nano /etc/crontab
# */9 * * * * update_ipsets.sh >/dev/null 2>&1


cd "$__dir/../blocklist-ipsets/"
git pull
curl -X POST --fail http://localhost:8080/private/refresh
