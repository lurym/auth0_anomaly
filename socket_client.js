'use strict';

var net = require('net'),
    ip = require('ip'),
    assert = require('assert'),
    async = require('async');

function sendMsg(msg, expected_result) {
    return function (cb) {
        var client = new net.Socket();
        client.connect(1337, '127.0.0.1', function () {
            console.log('Connected');
            if (ip.isV4Format(msg)) {
                client.write(ip.toBuffer(msg));
            } else {
                //sending anything else than 4 bytes makes service refresh the data
                client.write(new Buffer([0x01]));
            }
        });

        client.on('data', function (data) {
            assert.equal(data, expected_result);
            client.destroy(); // kill client after server's response
        });

        client.on('close', function () {
            console.log('Connection closed');
            cb();
        });
    };
}

async.series([
    sendMsg('127.0.0.1', 'cidr_report_bogons.netset'),
    sendMsg('1.2.4.5', 'valid'),
    sendMsg('whatever', 'refreshing'),
    function (cb) {
        console.log("TESTS PASSED");
        cb();
    }
]);
