'use strict';

var net = require('net'),
    ipstorage = require('./ipstorage'),
    ip = require('ip');

function checkIPAddress(data, socket) {
    var ipAddr = ip.toString(data),
        verResult = ipstorage.verifyIPAddress(ipAddr);
    if (verResult.blockedBy === null) {
        socket.write('valid');
    } else {
        socket.write(verResult.blockedBy);
    }
}

var server = net.createServer(function (socket) {
    socket.on('data', function (data) {
        if (data.length === 4) {
            checkIPAddress(data, socket);
        } else {
            ipstorage.refreshData();
            socket.write('refreshing');
        }
        socket.end();
    });
});

ipstorage.refreshData(function () {
    server.listen(1337, '0.0.0.0');
});
